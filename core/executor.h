#ifndef EXECUTOR_H
#define EXECUTOR_H

#include "core/chainlink.h"

class QMutex;

class Executor
{
public:
    using SizeType = quint32;

    explicit Executor(Chain chain, SizeType alignSize = 0);
    virtual ~Executor();

    void handleData(Data &&data);

private:
    Data localData;
    Chain chain;
    SizeType alignSize;
    QMutex *mutex;

    Result handleAll(PData &&pdata);
};

using PExecutor = QSharedPointer<Executor>;

#endif // EXECUTOR_H
