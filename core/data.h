#ifndef DATA_H
#define DATA_H

#include <QMetaType>
#include <QSharedPointer>

class Data
{
public:
    using Coordinate = qreal;
    using SamplingRate = quint32;

    struct Point {
        Coordinate x;
        Coordinate y;
        Point() : x(0), y(0) {}
        Point(Coordinate y) : x(0), y(y) {}
        Point(Coordinate x, Coordinate y) : x(x), y(y) {}
    };

    using Container = QList<Point>;

    explicit Data();
    Data(Container &&container, const SamplingRate &samplingRate);
    virtual ~Data();

    void append(const Data &data);

    Data::Container &getContainer();

    Data popFront(size_t size);

    size_t getSize() const;

    void normalize();

    SamplingRate getSamplingRate() const;

    friend QDebug operator<<(QDebug dbg, const Data &data);

private:
    Container container;

    SamplingRate samplingRate;
};

using PData = QSharedPointer<Data>;

Q_DECLARE_METATYPE(Data)
Q_DECLARE_METATYPE(PData)

QDebug operator<<(QDebug dbg, const Data::Point &point);
QDebug operator<<(QDebug dbg, const Data &data);

#endif // DATA_H
