#ifndef CHAINLINK
#define CHAINLINK

#include "core/basicstuff.h"
#include "core/data.h"
#include <QSharedPointer>
#include <QList>

class ChainLink
{
public:
    virtual Result handleData(PData pdata) = 0;
};

using PChainLink = QSharedPointer<ChainLink>;
using Chain = QList<PChainLink>;

#endif // CHAINLINK

