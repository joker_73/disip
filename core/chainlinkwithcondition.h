#ifndef CHAINLINKWITHCONDITION_H
#define CHAINLINKWITHCONDITION_H

#include "core/chainlink.h"
#include <QList>
#include <functional>

class ChainLinkWithCondition : public ChainLink
{
public:
    using CheckFunction = std::function<bool()>;

    void addCheckFunction(CheckFunction checkFunction);
    virtual Result handleDataWithCondition(PData pdata) = 0;

protected:
    bool checkAll();

private:
    QList<CheckFunction> checkFunctions;

    // ChainLink interface
    Result handleData(PData pdata) override;
};

#endif // CHAINLINKWITHCONDITION_H
