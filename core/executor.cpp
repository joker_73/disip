#include "core/executor.h"
#include <QMutex>

Executor::Executor(Chain chain, SizeType alignSize) :
    chain(chain),
    alignSize(alignSize),
    mutex(new QMutex)
{

}

Executor::~Executor()
{
    delete mutex;
}

void Executor::handleData(Data &&data)
{
    QMutexLocker locker(mutex);
    if (data.getSize() < 1) {
        qInfo() << "Executor: empty data";
        return;
    }
    if (alignSize > 0) {
        localData.append(data);
        while (static_cast<SizeType>(localData.getSize()) >= alignSize)
            handleAll(PData::create(localData.popFront(alignSize)));
    } else
        handleAll(PData::create(data));
}

Result Executor::handleAll(PData &&pdata)
{
    Result res = Result::START;
    for (auto chainLink : chain) {
        res = chainLink->handleData(pdata);
        switch (res) {
        case Result::OK:
        case Result::SKIP_ONE:
            break;
        case Result::SKIP:
            goto out;
        case Result::START:
        case Result::DONE:
        case Result::FAIL:
            qInfo("Result: [%s]", toStringResult(res));
            goto out;
        default:
            qWarning() << "Unknown result"
                     << ": [" << toStringResult(res) << "]";
        }
    }
out:
    return res;
}
