#include "core/chainlinkwithcondition.h"

void ChainLinkWithCondition::addCheckFunction(
        ChainLinkWithCondition::CheckFunction checkFunction)
{
    if (checkFunction)
        checkFunctions.append(checkFunction);
}

bool ChainLinkWithCondition::checkAll()
{
    for (auto checkFunction : checkFunctions)
        if (!checkFunction())
            return false;
    return true;
}

Result ChainLinkWithCondition::handleData(PData pdata)
{
    if (!checkAll())
        return Result::SKIP_ONE;
    return handleDataWithCondition(pdata);
}

