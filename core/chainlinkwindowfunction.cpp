#include "chainlinkwindowfunction.h"

Result ChainLinkWindowFunction::handleDataWithCondition(PData pdata)
{
    size_t size = pdata->getSize();
    int i = 0;
    for (auto &point : pdata->getContainer()) {
        point.y = point.y * calcWindow(i, size);
        ++i;
    }
    return Result::OK;
}
