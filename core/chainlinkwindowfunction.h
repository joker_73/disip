#ifndef CHAINLINKWINDOWFUNCTION_H
#define CHAINLINKWINDOWFUNCTION_H

#include "core/chainlinkwithcondition.h"

class ChainLinkWindowFunction : public ChainLinkWithCondition
{
public:
    // ChainLinkWithCondition interface
    virtual Result handleDataWithCondition(PData pdata) override;

protected:
    virtual Data::Coordinate calcWindow(int num, size_t size) = 0;
};

#endif // CHAINLINKWINDOWFUNCTION_H
