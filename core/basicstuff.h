#ifndef BASICSTUFF
#define BASICSTUFF

#include <QDebug>

#define MY_TEST(fmt, ...) \
    qDebug("MY_TEST:%s:%s:%d " #fmt, \
        __FILE__, __func__, __LINE__, ##__VA_ARGS__)

#define RETURN_CODE \
    X(START) \
    X(OK) \
    X(FAIL) \
    X(DONE) \
    X(SKIP) \
    X(SKIP_ONE)

enum class Result {
#define X(code) code,
    RETURN_CODE
#undef X
};

inline const char *toStringResult(Result code)
{
    const char *res = nullptr;
    switch (code) {
#define X(c) \
    case Result::c: \
        res = #c; \
        break;
    RETURN_CODE
#undef X
    }
    Q_ASSERT(res);
    return res;
}

#endif // BASICSTUFF
