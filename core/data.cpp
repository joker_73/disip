#include "core/data.h"
#include <QtAlgorithms>

#include "basicstuff.h"

QDebug operator<<(QDebug dbg, const Data::Point &point)
{
    dbg << "(" << point.x << point.y << ")";
    return dbg;
}

QDebug operator<<(QDebug dbg, const Data &data)
{
    dbg << data.samplingRate;
    for (auto point : data.container)
        dbg << point;
    return dbg;
}

namespace {
} /* namespace */

Data::Data() :
    samplingRate(0)
{
    qRegisterMetaType<Data>();
    qRegisterMetaType<PData>();
}

Data::Data(Container &&container, const SamplingRate &samplingRate) :
    samplingRate(samplingRate)
{
    this->container = std::move(container);
}

Data::~Data()
{

}

void Data::append(const Data &data)
{
    if (samplingRate == 0)
        samplingRate = data.samplingRate;
    else
        samplingRate = (samplingRate + data.samplingRate) / 2;
    container.append(data.container);
}

Data::Container &Data::getContainer()
{
    return container;
}

Data Data::popFront(size_t size)
{
    Data data;
    data.samplingRate = samplingRate;
    while (size-- > 0 && container.size() > 0)
        data.container.append(container.takeFirst());
    return data;
}

size_t Data::getSize() const
{
    return container.size();
}

void Data::normalize()
{
    quint64 value = 0;
    for (auto &i : container)
        i.x = value++;
}

Data::SamplingRate Data::getSamplingRate() const
{
    return samplingRate;
}
