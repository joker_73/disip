#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "chainlinks/datasender.h"
#include "chainlinks/notifier.h"
#include <QMainWindow>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_plot_curve.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnStart_clicked();
    void on_btnPlay_clicked();
    void on_actionChainSettings_triggered();
    void on_cbFourier_clicked();
    void on_cbToDb_clicked();

private:
    Ui::MainWindow *ui;

    QwtPlotGrid grid;

    QwtPlotCurve curvBlue;
    QVector<qreal> aXBlue, aYBlue;

    QwtPlotCurve curvRed;
    QVector<qreal> aXRed, aYRed;

    PDataSender plotCurvBlue;
    PDataSender plotCurvRed;
    PDataSender printer;
    PNotifier replot;
    PNotifier stats;

    QString textOfBtnStart;
    QString textOfBtnPlay;
    QDialog *chainSettings;

    void setAxis();
    void clearQwtPlot();
    Chain getFullChain(quint32 blockSize);
    Chain getSimplyChain();
};

#endif // MAINWINDOW_H
