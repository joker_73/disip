#ifndef DROP_H
#define DROP_H

#include "core/chainlink.h"

class Drop : public ChainLink
{
public:
    explicit Drop(quint8 drop);
    virtual ~Drop();

    // ChainLink interface
    virtual Result handleData(PData pdata) override;

private:
    quint8 drop;
    quint8 count;
};

using PDrop = QSharedPointer<Drop>;

#endif // DROP_H
