#include "chainlinks/drop.h"

Drop::Drop(quint8 drop) :
    drop(drop),
    count(1)
{

}

Drop::~Drop()
{

}

Result Drop::handleData(PData pdata)
{
    Q_UNUSED(pdata);
    if (count++ % drop == 0)
        return Result::OK;
    return Result::SKIP;
}

