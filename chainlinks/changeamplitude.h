#ifndef CHANGEAMPLITUDE_H
#define CHANGEAMPLITUDE_H

#include "core/chainlink.h"

class ChangeAmplitude : public ChainLink
{
public:
    explicit ChangeAmplitude(qreal factor);
    virtual ~ChangeAmplitude();

    // ChainLink interface
    virtual Result handleData(PData pdata) override;

private:
    qreal factor;
};

using PChangeAmplitude = QSharedPointer<ChangeAmplitude>;

#endif // CHANGEAMPLITUDE_H
