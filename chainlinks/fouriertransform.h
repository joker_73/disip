#ifndef FOURIERTRANSFORM_H
#define FOURIERTRANSFORM_H

#include "core/chainlinkwithcondition.h"
#include <fftw3.h>

class FourierTransform : public ChainLinkWithCondition
{
public:
    explicit FourierTransform(quint32 blockSize,
                              CheckFunction checkFunction = nullptr);
    virtual ~FourierTransform();

    // ChainLinkWithCondition interface
    virtual Result handleDataWithCondition(PData pdata) override;

private:
    quint32 blockSize;

    fftw_complex *in;
    fftw_complex *out;
    fftw_plan plan;
};

using PFourierTransform = QSharedPointer<FourierTransform>;

#endif // FOURIERTRANSFORM_H
