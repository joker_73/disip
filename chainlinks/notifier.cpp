#include "chainlinks/notifier.h"

Notifier::Notifier(QObject *parent) : QObject(parent)
{

}

Notifier::~Notifier()
{

}

Result Notifier::handleData(PData pdata)
{
    Q_UNUSED(pdata);
    emit notify();
    return Result::OK;
}

