#include "chainlinks/fouriertransform.h"
#include <complex>

FourierTransform::FourierTransform(quint32 blockSize,
                                   CheckFunction checkFunction) :
    blockSize(blockSize)
{
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * blockSize);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * blockSize);
    plan = fftw_plan_dft_1d(blockSize, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    addCheckFunction(checkFunction);
}

FourierTransform::~FourierTransform()
{
    fftw_free(in);
    fftw_free(out);
    fftw_destroy_plan(plan);
}

Result FourierTransform::handleDataWithCondition(PData pdata)
{
    auto &container = pdata->getContainer();

    memset(in, 0, blockSize * sizeof(fftw_complex));

    {
        quint32 count = 0;
        for (auto point : container)
            in[count++][1] = point.y;
    }

    fftw_execute(plan);

    size_t size = pdata->getSize();
    container.clear();
    qreal frequencyStep = (qreal)pdata->getSamplingRate() / size;
    for (size_t i = 1; i < size / 2; ++i) {
        std::complex<double> c{out[i][0], out[i][1]};
        Data::Point point;
        point.x = static_cast<Data::Coordinate>(frequencyStep * i);
        point.y = static_cast<Data::Coordinate>(std::abs(c));
        container.append(std::move(point));
    }
    return Result::OK;
}
