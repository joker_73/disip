#ifndef WINDOWHAMMING_H
#define WINDOWHAMMING_H

#include "core/chainlinkwindowfunction.h"

// Hamming window function
class WindowHamming : public ChainLinkWindowFunction
{
public:
    explicit WindowHamming(CheckFunction checkFunction = nullptr);
    virtual ~WindowHamming();

protected:
    // ChainLinkWindowFunction interface
    virtual Data::Coordinate calcWindow(int num, size_t size) override;
};

using PWindowHamming = QSharedPointer<WindowHamming>;

#endif // WINDOWHAMMING_H
