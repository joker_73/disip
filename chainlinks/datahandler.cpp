#include "datahandler.h"

DataHandler::DataHandler(Handler handler) :
    handler(handler)
{

}

DataHandler::~DataHandler()
{

}

Result DataHandler::handleData(PData pdata)
{
    if (handler)
        return handler(pdata);
    return Result::FAIL;
}
