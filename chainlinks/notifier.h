#ifndef NOTIFIER_H
#define NOTIFIER_H

#include "core/chainlink.h"

class Notifier : public QObject, public ChainLink
{
    Q_OBJECT
public:
    explicit Notifier(QObject *parent = 0);
    virtual ~Notifier();

    // ChainLink interface
    virtual Result handleData(PData pdata) override;

signals:
    void notify();

public slots:

};

using PNotifier = QSharedPointer<Notifier>;

#endif // NOTIFIER_H
