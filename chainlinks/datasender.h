#ifndef DATASENDER_H
#define DATASENDER_H

#include "core/chainlink.h"
#include <QObject>

class DataSender : public QObject, public ChainLink
{
    Q_OBJECT
public:
    explicit DataSender(QObject *parent = 0);
    virtual ~DataSender();

    // ChainLink interface
    virtual Result handleData(PData pdata) override;

signals:
    /*
     * If connected slot using pdata then ConnectionType must be
     * Qt::BlockingQueuedConnection!
     */
    void send(PData pdata);

public slots:

};

using PDataSender = QSharedPointer<DataSender>;

#endif // DATASENDER_H
