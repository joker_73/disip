#ifndef WINDOWBLACKMAN_H
#define WINDOWBLACKMAN_H

#include "core/chainlinkwindowfunction.h"

// Blackman window function
class WindowBlackman : public ChainLinkWindowFunction
{
public:
    explicit WindowBlackman(CheckFunction checkFunction = nullptr);
    virtual ~WindowBlackman();

protected:
    // ChainLinkWindowFunction interface
    virtual Data::Coordinate calcWindow(int num, size_t size) override;
};

using PWindowBlackman = QSharedPointer<WindowBlackman>;

#endif // WINDOWBLACKMAN_H
