#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include "core/chainlink.h"
#include <functional>

class DataHandler : public ChainLink
{
public:
    using Handler = std::function<Result(PData)>;

    explicit DataHandler(Handler handler);
    virtual ~DataHandler();

    // ChainLink interface
    virtual Result handleData(PData pdata) override;

private:
    Handler handler;
};

using PDataHandler = QSharedPointer<DataHandler>;

#endif // DATAHANDLER_H
