#include "chainlinks/changeamplitude.h"
using std::begin;
using std::end;

ChangeAmplitude::ChangeAmplitude(qreal factor) :
    factor(factor)
{

}

ChangeAmplitude::~ChangeAmplitude()
{

}

Result ChangeAmplitude::handleData(PData pdata)
{
    for (auto &point : pdata->getContainer())
        point.y *= factor;
    return Result::OK;
}
