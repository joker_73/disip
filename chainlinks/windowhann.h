#ifndef WINDOWHANN_H
#define WINDOWHANN_H

#include "core/chainlinkwindowfunction.h"

// Hann (Hanning) window function
class WindowHann : public ChainLinkWindowFunction
{
public:
    explicit WindowHann(CheckFunction checkFunction = nullptr);
    virtual ~WindowHann();

protected:
    // ChainLinkWindowFunction interface
    virtual Data::Coordinate calcWindow(int num, size_t size) override;
};

using PWindowHann = QSharedPointer<WindowHann>;

#endif // WINDOWHANN_H
