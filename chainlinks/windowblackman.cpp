#include "windowblackman.h"
#include <QtMath>

WindowBlackman::WindowBlackman(CheckFunction checkFunction)
{
    addCheckFunction(checkFunction);
}

WindowBlackman::~WindowBlackman()
{

}

Data::Coordinate WindowBlackman::calcWindow(int num, size_t size)
{
    return 0.42 - 0.5 * cos(2 * M_PI * num / size) + 0.8 * cos(4 * M_PI * num / size);
}
