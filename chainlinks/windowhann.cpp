#include "windowhann.h"
#include <QtMath>

WindowHann::WindowHann(CheckFunction checkFunction)
{
    addCheckFunction(checkFunction);
}

WindowHann::~WindowHann()
{

}

Data::Coordinate WindowHann::calcWindow(int num, size_t size)
{
    return 0.5 * (1 - cos(2 * M_PI * num / size));
}
