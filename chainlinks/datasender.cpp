#include "chainlinks/datasender.h"

DataSender::DataSender(QObject *parent) : QObject(parent)
{

}

DataSender::~DataSender()
{

}

Result DataSender::handleData(PData pdata)
{
    emit send(pdata);
    return Result::OK;
}
