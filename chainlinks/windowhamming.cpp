#include "windowhamming.h"
#include <QtMath>

WindowHamming::WindowHamming(CheckFunction checkFunction)
{
    addCheckFunction(checkFunction);
}

WindowHamming::~WindowHamming()
{

}

Data::Coordinate WindowHamming::calcWindow(int num, size_t size)
{
    return 0.54 - 0.46 * cos(2 * M_PI * num / size);
}
