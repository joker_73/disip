#include "chainsettings.h"
#include "ui_chainsettings.h"

ChainSettings::ChainSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChainSettings)
{
    ui->setupUi(this);
}

ChainSettings::~ChainSettings()
{
    delete ui;
}
