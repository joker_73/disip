#include "datasources/singenerator.h"
#include <QThread>
#include <QMutex>
#include <QtMath>

namespace {
class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(PExecutor pexecutor, size_t size, qreal coefficient,
                    unsigned long delayMs, qreal deltaSwipCoefficient,
                    qreal deltaPhase, QObject *parent = 0);
    virtual ~Worker();

    bool isRunning();

signals:
    void changeState(bool running);
    void sendData(Data data);
    void finished();

public slots:
    void start();
    void stop();

private:
    QMutex mutex;
    PExecutor pexecutor;
    size_t size;
    qreal coefficient;
    unsigned long delayMs;
    qreal deltaSwipCoefficient;
    qreal deltaPhase;

    qreal swipCoefficient;
    qreal phase;
    bool running;

    Data getData();
    void setRunning(bool running);
};

Worker::Worker(PExecutor pexecutor, size_t size, qreal coefficient,
               unsigned long delayMs, qreal deltaSwipCoefficient,
               qreal deltaPhase, QObject *parent) :
    QObject(parent),
    pexecutor(pexecutor),
    size(size),
    coefficient(coefficient),
    delayMs(delayMs),
    deltaSwipCoefficient(deltaSwipCoefficient),
    deltaPhase(deltaPhase),
    swipCoefficient(1),
    phase(0),
    running(false)
{

}

Worker::~Worker()
{

}

bool Worker::isRunning()
{
    QMutexLocker locker(&mutex);
    return running;
}

void Worker::start()
{
    if (isRunning()) {
        qWarning() << "SinGenerator alredy running";
        return;
    }

    setRunning(true);
    while (isRunning()) {
        pexecutor->handleData(getData());
        QThread::msleep(delayMs);
    }
    emit finished();
}

void Worker::stop()
{
    setRunning(false);
}

Data Worker::getData()
{
    Data::Container container;
    for (size_t i = 0; i < size; ++i) {
        qreal x = coefficient * swipCoefficient * i;
        auto p = Data::Point{static_cast<Data::Coordinate>(i), sin(x + phase)};
        container.append(p);
    }
    swipCoefficient += deltaSwipCoefficient;
    phase += deltaPhase;
    return Data{std::move(container), 0};
}

void Worker::setRunning(bool running)
{
    QMutexLocker locker(&mutex);
    this->running = running;
    emit changeState(running);
}
} /* namespace */

SinGenerator::SinGenerator(PExecutor pexecutor, size_t size, qreal coefficient,
                           unsigned long delayMs, qreal deltaSwipCoefficient,
                           qreal deltaPhase, QObject *parent) :
    QObject(parent),
    running(false)
{
    Q_ASSERT(!pexecutor.isNull());
    Q_ASSERT(coefficient != 0);
    thread = new QThread(this);
    Worker *worker = new Worker{pexecutor, size, coefficient, delayMs,
            deltaSwipCoefficient, deltaPhase};
    worker->moveToThread(thread);
    connect(thread, &QThread::finished, worker, &Worker::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);
    connect(this, &SinGenerator::startWorker, worker, &Worker::start);
    connect(this, &SinGenerator::stopWorker, [=]() {
        worker->stop();
    });
    connect(worker, &Worker::changeState, this, [&](bool running) {
        this->running = running;
    });
    connect(worker, &Worker::finished, this, &SinGenerator::finished);
    thread->start();
}

SinGenerator::~SinGenerator()
{
    thread->quit();
    thread->wait();
}

bool SinGenerator::isRunning()
{
    return running;
}

void SinGenerator::start()
{
    emit startWorker();
}

void SinGenerator::stop()
{
    emit stopWorker();
}

#include "singenerator.moc"
