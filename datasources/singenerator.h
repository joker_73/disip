#ifndef SINGENERATOR_H
#define SINGENERATOR_H

#include "core/executor.h"
#include <QObject>

class QThread;

class SinGenerator : public QObject
{
    Q_OBJECT
public:
    explicit SinGenerator(PExecutor pexecutor, size_t size,
                          qreal coefficient = 1, unsigned long delayMs = 0,
                          qreal deltaSwipCoefficient = 0, qreal deltaPhase = 0,
                          QObject *parent = 0);
    virtual ~SinGenerator();

    bool isRunning();

signals:
    void finished();
    void startWorker();
    void stopWorker();

public slots:
    void start();
    void stop();

private:
    QThread *thread;
    bool running;
};

#endif // SINGENERATOR_H
