#include "datasources/microphone.h"
#include <QAudioInput>
#include <QDataStream>
#include <QThread>
#include <functional>

namespace {
class Worker : public QObject
{
    Q_OBJECT
public:
    Worker(PExecutor pexecutor, QObject *parent = 0);
    virtual ~Worker();

signals:
    void changeState(bool running);
    void sendData(Data data);
    void finished();

public slots:
    void start();
    void stop();

private:
    QAudioFormat audioFormat;
    PExecutor pexecutor;
    QAudioInput *audioInput;

    bool isRunning();
};

Worker::Worker(PExecutor pexecutor, QObject *parent) :
    QObject(parent),
    pexecutor(pexecutor),
    audioInput(nullptr)
{
    audioFormat.setSampleRate(
//                8000
//                16000
                44100
//                48000
//                96000
                );
    audioFormat.setChannelCount(1);
    audioFormat.setCodec("audio/pcm");
    audioFormat.setSampleSize(32);
//    audioFormat.setSampleSize(16);
//    audioFormat.setSampleSize(8);
//    audioFormat.setByteOrder(QAudioFormat::LittleEndian);
//    audioFormat.setSampleType(QAudioFormat::UnSignedInt);
//    audioFormat.setSampleType(QAudioFormat::SignedInt);
    audioFormat.setSampleType(QAudioFormat::Float);
    QAudioDeviceInfo audioDeviceInfo = QAudioDeviceInfo::defaultInputDevice();
    if (!audioDeviceInfo.isFormatSupported(audioFormat)) {
        qWarning() << "Default format not supported, trying to use the nearest";
        audioFormat = audioDeviceInfo.nearestFormat(audioFormat);
    }

    qInfo() << audioFormat;
}

Worker::~Worker()
{
    if (audioInput)
        delete audioInput;
}

bool Worker::isRunning()
{
    if (audioInput) {
        QAudio::State state = audioInput->state();
        if (state == QAudio::ActiveState || state == QAudio::IdleState)
            return true;
    }
    return false;
}

void Worker::start()
{
    if (isRunning()) {
        qWarning() << "Microphone alredy running";
        return;
    }

    audioInput = new QAudioInput(audioFormat, this);
    connect(audioInput, &QAudioInput::stateChanged, [&](QAudio::State state) {
        emit changeState(isRunning());
        if (state != QAudio::StoppedState)
            return;
        QAudio::Error error = audioInput->error();
        if (error != QAudio::NoError)
            qWarning() << "QAudio error: " << error;
        emit finished();
    });

    Data::SamplingRate samplingRate;
    samplingRate = static_cast<Data::SamplingRate>(audioFormat.sampleRate());
    QDataStream::ByteOrder byteOrder = QDataStream::LittleEndian;
    if (audioFormat.byteOrder() != QAudioFormat::LittleEndian)
        byteOrder = QDataStream::BigEndian;
    int sampleSize = audioFormat.sampleSize();
    QAudioFormat::SampleType sampleType = audioFormat.sampleType();

    std::function<Data::Coordinate(QDataStream &stream)> read;
    if (sampleSize == 16 && sampleType == QAudioFormat::UnSignedInt) {
        read = [](QDataStream &stream) {
            quint16 value;
            stream >> value;
            Data::Coordinate coordinate = static_cast<Data::Coordinate>(value);
            coordinate /= 0x8000;
            coordinate -= 1;
            return coordinate;
        };
    } else if (sampleSize == 16 && sampleType == QAudioFormat::SignedInt) {
        read = [](QDataStream &stream) {
            qint16 value;
            stream >> value;
            Data::Coordinate coordinate = static_cast<Data::Coordinate>(value);
            coordinate /= 0x8000;
            return coordinate;
        };
    } else if (sampleSize == 8 && sampleType == QAudioFormat::UnSignedInt) {
        read = [](QDataStream &stream) {
            quint8 value;
            stream >> value;
            Data::Coordinate coordinate = static_cast<Data::Coordinate>(value);
            coordinate /= 0x80;
            coordinate -= 1;
            return coordinate;
        };
    } else if (sampleSize == 8 && sampleType == QAudioFormat::SignedInt) {
        read = [](QDataStream &stream) {
            qint8 value;
            stream >> value;
            Data::Coordinate coordinate = static_cast<Data::Coordinate>(value);
            coordinate /= 0x80;
            return coordinate;
        };
    } else if (sampleType == QAudioFormat::Float) {
        read = [](QDataStream &stream) {
            float v;
            stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
            stream >> v;
            return static_cast<Data::Coordinate>(v);
        };
    }

    QIODevice *device = audioInput->start();
    connect(device, &QIODevice::readyRead, [=]() {
        Data::Container container;
        QByteArray buffer = device->readAll();
        QDataStream stream{buffer};
        stream.setByteOrder(byteOrder);
        while (!stream.atEnd())
            container.append(std::move(Data::Point{read(stream)}));
        pexecutor->handleData(Data{std::move(container), samplingRate});
    });
}

void Worker::stop()
{
    audioInput->stop();
}
} /* namespace */

Microphone::Microphone(PExecutor pexecutor, QObject *parent) :
    QObject(parent),
    thread(new QThread{this}),
    running(false)
{
    Q_ASSERT(!pexecutor.isNull());
    Worker *worker = new Worker{pexecutor};
    worker->moveToThread(thread);
    connect(thread, &QThread::finished, worker, &Worker::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);
    connect(this, &Microphone::startWorker, worker, &Worker::start);
    connect(this, &Microphone::stopWorker, worker, &Worker::stop);
    connect(worker, &Worker::changeState, this, [&](bool running) {
        this->running = running;
    });
    connect(worker, &Worker::finished, this, [&]() {
        emit finished();
    });
    thread->start();
}

Microphone::~Microphone()
{
    thread->quit();
    thread->wait();
}

bool Microphone::isRunning()
{
    return running;
}

void Microphone::start()
{
    emit startWorker();
}

void Microphone::stop()
{
    emit stopWorker();
}

#include "microphone.moc"
