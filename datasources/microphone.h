#ifndef MICROPHONE_H
#define MICROPHONE_H

#include "core/executor.h"
#include <QObject>

class QThread;

class Microphone : public QObject
{
    Q_OBJECT
public:
    explicit Microphone(PExecutor pexecutor, QObject *parent = 0);
    virtual ~Microphone();

    bool isRunning();

signals:
    void finished();
    void startWorker();
    void stopWorker();

public slots:
    void start();
    void stop();

private:
    QThread *thread;
    bool running;
};

#endif // MICROPHONE_H
