#ifndef CHAINSETTINGS_H
#define CHAINSETTINGS_H

#include <QDialog>

namespace Ui {
class ChainSettings;
}

class ChainSettings : public QDialog
{
    Q_OBJECT

public:
    explicit ChainSettings(QWidget *parent = 0);
    ~ChainSettings();

private:
    Ui::ChainSettings *ui;
};

#endif // CHAINSETTINGS_H
