#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "chainsettings.h"
#include "core/executor.h"
#include "datasources/microphone.h"
#include "datasources/singenerator.h"
#include "chainlinks/changeamplitude.h"
#include "chainlinks/datahandler.h"
#include "chainlinks/drop.h"
#include "chainlinks/fouriertransform.h"
#include "chainlinks/windowblackman.h"
#include "chainlinks/windowhamming.h"
#include "chainlinks/windowhann.h"
#include <QScroller>
#include <QThread>
#include <QDateTime>
#include <QStatusBar>
#include <qwt/qwt_scale_engine.h>
#include <qwt/qwt_symbol.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    chainSettings(new ChainSettings)
{
    ui->setupUi(this);

#ifdef Q_OS_ANDROID
    QScroller::grabGesture(ui->saSettings, QScroller::LeftMouseButtonGesture);
    ui->saSettings->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->saSettings->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
#else
    statusBar();
#endif /* Q_OS_ANDROID */

    chainSettings->setModal(true);

    grid.enableXMin(true);
    grid.setMajorPen(QPen(Qt::black, 0, Qt::DotLine));
    grid.setMinorPen(QPen(Qt::gray,  0, Qt::DotLine));
    grid.attach(ui->qwtPlot);

    curvBlue.setRenderHint(QwtPlotItem::RenderAntialiased);
    curvBlue.setPen(QPen(Qt::blue));
    curvBlue.attach(ui->qwtPlot);

    curvRed.setRenderHint(QwtPlotItem::RenderAntialiased);
    curvRed.setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::yellow,
                                    QPen(Qt::red), QSize(5, 5)));
    curvRed.setPen(QPen(Qt::red));
    curvRed.setStyle(QwtPlotCurve::Sticks);
    curvRed.attach(ui->qwtPlot);

    textOfBtnStart = ui->btnStart->text();
    textOfBtnPlay = ui->btnPlay->text();

    plotCurvBlue = PDataSender::create();
    connect(plotCurvBlue.data(), &DataSender::send, this, [this](PData pdata) {
        aXBlue.clear();
        aYBlue.clear();
        for (auto point : pdata->getContainer()) {
            aXBlue.append(point.x);
            aYBlue.append(point.y);
        }
        curvBlue.setSamples(aXBlue, aYBlue);
    }, Qt::BlockingQueuedConnection);

    plotCurvRed = PDataSender::create();
    connect(plotCurvRed.data(), &DataSender::send, this, [this](PData pdata) {
        aXRed.clear();
        aYRed.clear();
        for (auto point : pdata->getContainer()) {
            aXRed.append(point.x);
            aYRed.append(point.y);
        }
        curvRed.setSamples(aXRed, aYRed);
    }, Qt::BlockingQueuedConnection);

    printer = PDataSender::create();
    connect(printer.data(), &DataSender::send, this, [this](PData pdata) {
        QString str;
        for (auto point : pdata->getContainer())
            str.append(QString("%1; ").arg(point.y));
        qDebug() << str;
    }, Qt::BlockingQueuedConnection);

    replot = PNotifier::create();
    connect(replot.data(), &Notifier::notify, this, [this]() {
        ui->qwtPlot->replot();
    });

    stats = PNotifier::create();
    connect(stats.data(), &Notifier::notify, this, [this]() {
        static const quint16 SIZE = 50;
        static quint64 num = 0;
        static qint64 priv = QDateTime::currentMSecsSinceEpoch();
        static decltype(priv) arr[SIZE] = {0};

        arr[num % SIZE] = QDateTime::currentMSecsSinceEpoch() - priv;

        decltype(priv) time = 0;
        for (auto i : arr)
            time += i;
        time /= SIZE;

        auto message = QString("Period: %1 ms / Total number: %2")
                .arg(time).arg(++num);

        ui->labelInfo->setText(message);
#ifndef Q_OS_ANDROID
        statusBar()->showMessage(message);
#endif /* !Q_OS_ANDROID */

        priv = QDateTime::currentMSecsSinceEpoch();
    });
}

MainWindow::~MainWindow()
{
    delete chainSettings;
    delete ui;
}

void MainWindow::on_btnStart_clicked()
{
    static SinGenerator *sinGenerator = nullptr;
    if (sinGenerator && sinGenerator->isRunning()) {
        sinGenerator->stop();
        ui->btnStart->setText(textOfBtnStart);
    } else {
        clearQwtPlot();
        ui->qwtPlot->setAxisScaleEngine(QwtPlot::xBottom, new QwtLinearScaleEngine);
        ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, 32);
        ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -1.05, 1.05);

        auto pexecutor = PExecutor::create(getSimplyChain());
        sinGenerator = new SinGenerator{std::move(pexecutor), 32, 0.3, 25, 0, 0.01};

        connect(sinGenerator, &SinGenerator::finished, [=]() {
            sinGenerator->deleteLater();
            sinGenerator = nullptr;
            qInfo("SinGenerator is finished");
        });
        sinGenerator->start();

        ui->btnStart->setText("Stop (sin)");
    }
}

void MainWindow::on_btnPlay_clicked()
{
    static Microphone *microphone = nullptr;
    if (microphone && microphone->isRunning()) {
        microphone->stop();
        ui->btnPlay->setText(textOfBtnPlay);
    } else {
        clearQwtPlot();
        setAxis();

        int blockSize = 1 << 11;
        auto pexecutor = PExecutor::create(getFullChain(blockSize), blockSize);
        microphone = new Microphone{std::move(pexecutor)};
        connect(microphone, &Microphone::finished, this, [=]() {
            microphone->deleteLater();
            microphone = nullptr;
            qInfo("Microphone is finished");
        });
        microphone->start();

        ui->btnPlay->setText("Stop (mic)");
    }
}

void MainWindow::setAxis()
{
    if (!ui->cbFourier->isChecked()) {
        ui->qwtPlot->setAxisScaleEngine(QwtPlot::xBottom, new QwtLinearScaleEngine);
        ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, (1 << 11) * 1.01);
        ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -1.05, 1.05);
        return;
    }

    if (ui->cbToDb->isChecked()) {
        ui->qwtPlot->setAxisScaleEngine(QwtPlot::xBottom, new QwtLogScaleEngine(10));
        ui->qwtPlot->setAxisAutoScale(QwtPlot::xBottom);
        ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -40, 20);
    } else {
        ui->qwtPlot->setAxisScaleEngine(QwtPlot::xBottom, new QwtLinearScaleEngine);
        ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, 22000 * 1.01);
        ui->qwtPlot->setAxisScale(QwtPlot::yLeft, 0, 4);
    }
}

void MainWindow::clearQwtPlot()
{
    aXBlue.clear();
    aYBlue.clear();
    aXRed.clear();
    aYRed.clear();
    curvBlue.setSamples(aXBlue, aYBlue);
    curvRed.setSamples(aXRed, aYRed);
    ui->qwtPlot->replot();
}

Chain MainWindow::getFullChain(quint32 blockSize)
{
    Chain chain;
    chain.append(PDataHandler::create([&](PData pdata)->Result {
        if (ui->cbFourier->isChecked())
            return Result::SKIP_ONE;
        pdata->normalize();
        return Result::OK;
    }));
    chain.append(PWindowBlackman::create([&]() -> bool {
        return ui->rbWindowBlackman->isChecked() && ui->cbFourier->isChecked();
    }));
    chain.append(PWindowHamming::create([&]() -> bool {
        return ui->rbWindowHamming->isChecked() && ui->cbFourier->isChecked();
    }));
    chain.append(PWindowHann::create([&]() -> bool {
        return ui->rbWindowHann->isChecked() && ui->cbFourier->isChecked();
    }));
    chain.append(PFourierTransform::create(blockSize, [&]() -> bool {
        return ui->cbFourier->isChecked();
    }));
    chain.append(PDataHandler::create([&](PData pdata) -> Result {
        // Conversion to decibels
        if (!ui->cbToDb->isChecked() || !ui->cbFourier->isChecked())
            return Result::SKIP_ONE;
        for (auto &point : pdata->getContainer())
            point.y = 10 * log10(point.y / ui->doubleSpinBoxDbBase->value());
        return Result::OK;
    }));
    chain.append(plotCurvBlue);
    chain.append(replot);
    chain.append(stats);
    return chain;
}

Chain MainWindow::getSimplyChain()
{
    Chain chain;
    chain.append(plotCurvBlue);
    chain.append(PChangeAmplitude::create(0.5));
    chain.append(plotCurvRed);
    chain.append(replot);
    chain.append(stats);
    return chain;
}

void MainWindow::on_actionChainSettings_triggered()
{
    chainSettings->show();
}

void MainWindow::on_cbFourier_clicked()
{
    setAxis();
}

void MainWindow::on_cbToDb_clicked()
{
    setAxis();
}
