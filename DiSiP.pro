#-------------------------------------------------
#
# Project created by QtCreator 2015-11-12T19:35:14
#
#-------------------------------------------------

QT       += core gui multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 qwt

TARGET = DiSiP
TEMPLATE = app

QMAKE_LIBDIR_OPENVG

SOURCES += main.cpp\
    mainwindow.cpp \
    chainsettings.cpp \
    core/chainlinkwithcondition.cpp \
    core/data.cpp \
    core/executor.cpp \
    datasources/microphone.cpp \
    datasources/singenerator.cpp \
    chainlinks/changeamplitude.cpp \
    chainlinks/datasender.cpp \
    chainlinks/drop.cpp \
    chainlinks/fouriertransform.cpp \
    chainlinks/notifier.cpp \
    chainlinks/datahandler.cpp \
    chainlinks/windowblackman.cpp \
    chainlinks/windowhamming.cpp \
    chainlinks/windowhann.cpp \
    core/chainlinkwindowfunction.cpp

HEADERS  += mainwindow.h \
    chainsettings.h \
    core/basicstuff.h \
    core/chainlink.h \
    core/chainlinkwithcondition.h \
    core/data.h \
    core/executor.h \
    datasources/microphone.h \
    datasources/singenerator.h \
    chainlinks/changeamplitude.h \
    chainlinks/datasender.h \
    chainlinks/drop.h \
    chainlinks/fouriertransform.h \
    chainlinks/notifier.h \
    chainlinks/datahandler.h \
    chainlinks/windowblackman.h \
    chainlinks/windowhamming.h \
    chainlinks/windowhann.h \
    core/chainlinkwindowfunction.h

FORMS    += mainwindow.ui \
    chainsettings.ui

android {
    INCLUDEPATH += platforms/armeabi-v7a/include \
        platforms/armeabi-v7a/include/qwt

    # In this case not work reading dependencies becouse need copy this
    # libraries to <qt_root>/<version>/<platform>/lib (qt/5.5/android_armv7/lib)
    #LIBS += -L$$PWD/platforms/armeabi-v7a/lib -lqwt -l:libfftw3.a
    LIBS += -lqwt -l:libfftw3.a

    DISTFILES += \
        android/AndroidManifest.xml \
        android/gradle/wrapper/gradle-wrapper.jar \
        android/gradlew \
        android/res/values/libs.xml \
        android/build.gradle \
        android/gradle/wrapper/gradle-wrapper.properties \
        android/gradlew.bat

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
} else:unix {
    LIBS += -lfftw3
}
